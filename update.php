<?php
header('Cache-Control: no-cache, must-revalidate, private, max-age=0');
require './config.php';
require './Cloudflare.php';

function get_ip_address() {
    $ip_keys = array('HTTP_CF_CONNECTING_IP', 'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
    foreach ($ip_keys as $key) {
        if (array_key_exists($key, $_SERVER) === true) {
            foreach (explode(',', $_SERVER[$key]) as $ip) {
                // trim for safety measures
                $ip = trim($ip);
                // attempt to validate IP
                if (validate_ip($ip)) {
                    return $ip;
                }
            }
        }
    }
    return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
}

/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip)
{
    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_IPV6 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
        return false;
    }
    return true;
}

$api = new Cloudflare($config['email'], $config['api_key']);

if(!isset($_GET["domain"]) || !isset($_GET["name"]) || !isset($_GET["auth"])) {
  header("HTTP/1.0 400 Missing Data");
  print "Error, missing data.\n";
  exit;
}

if(!in_array($_GET["auth"], $config['auth_keys'])) {
  header("HTTP/1.0 403 Forbidden");
  print "No authorization.\n";
  exit;
}

$domain = $_GET["domain"];
$name   = $_GET["name"];
$host   = $_GET["name"] . "." . $_GET["domain"];

$ip = get_ip_address();

$ipClass = (preg_match('/^[A-Fa-f0-9:]+$/', $ip) ? 'AAAA' : 'A');
$ttl = (isset($_GET["ttl"])?$_GET["ttl"]:$config['ttl']);

try
{
  $zone = $api->getZone($domain);
  if (!$zone)
  {
    header("HTTP/1.0 404 Domain not found");
    print "Domain $domain not found\n";
    exit;
  }

  $records = $api->getZoneDnsRecords($zone['id'], ['type' => $ipClass, 'name' => $name]);
  $record  = $records && $records[0]['name'] == $recordName ? $records[0] : null;
  if (!$record)
  {
    if ($verbose) echo "No existing record found. Creating a new one\n";
    $ret = $api->createDnsRecord($zone['id'], $ipClass, $name, $ip, ['ttl' => $ttl]);
  }
  elseif (($record['type'] != 'A' && $record['type'] != 'AAAA') || $record['content'] != $ip || $record['ttl'] != $config['ttl'])
  {
    if ($verbose) echo "Updating record.\n";
    $ret = $api->updateDnsRecord($zone['id'], $record['id'], [
      'type'    => $ipClass,
      'name'    => $name,
      'content' => $ip,
      'ttl'     => $ttl,
    ]);
  }
  if($ipClass === 'AAAA' && isset($_GET["dualstack"]) && !empty($config['ipv4Host'])) {
    header("HTTP/1.1 302 Try IPv4");
    header("Location: ".$config['ipv4Host'].$_SERVER["REQUEST_URI"]);
  }
  print "$ip\n";
  return 0;
}
catch (Exception $e)
{
  echo "Error: " . $e->getMessage() . "\n";
  return 1;
}

This is a very simple PHP server side script to update hosts to Cloudflare DNS automatically.

Place on a server and copy `config.php.example` to `config.php`. Add your Cloudflare keys.

For dualstack (IPv4 and IPv6) support, you'll need a DNS record that only resolves to IPv4 and place that in the config file.

On the client side, the request URL is sent using GET with the following options:

* `domain` - Domain to put the hostname into
* `name` - Hostname (will become name.domain)
* `ttl` - Optional, set the TTL for the record (1 is automatic, fixed should be higher than 120)
* `dualstack` - Set to any value to enable dual stack redirection (automatically redirect client to IPv4 only page if an IPv6 address is detected)
* `auth` - One of the auth keys from the config file.
